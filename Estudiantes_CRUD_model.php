
<?php
 class Estudiantes_CRUD_model extends CI_Model {
 
 function Estudiantes_CRUD_model() {
 parent::__construct(); //llamada al constructor de Model.
 }
 
function getData() {
 $estudiantes = $this->db->get("student"); //obtenemos la tabla 'contacto'. db->get('nombre_tabla') equivale a SELECT * FROM nombre_tabla.
 
 return $estudiantes->result(); //devolvemos el resultado de lanzar la query.
 }
 function insert($data) {
 $this->db->set('nombre', $data['nombre']);
 $this->db->set('apellido', $data['apellido']);
 $this->db->set('password', $data['password']);
  $this->db->set('cedula', $data['cedula']);
 $this->db->set('is_admin', $data['is_admin']);
 $this->db->set('email', $data['email']);
 $this->db->insert('student');
 }

 function borrar ($id) {
 $this->db->where('id', $id);
 $this->db->delete('student');
 }

 function obtenerContacto($id) {
$this->db->select('id,nombre,apellido,password,cedula,is_admin,email');
$this->db->from('student');
$this->db->where('id = ' . $id);
$contacto = $this->db->get();
return $contacto->result();
}
function update($data) {
 $this->db->set('nombre', $data['nombre']);
 $this->db->set('apellido', $data['apellido']);
 $this->db->set('password', $data['password']);
  $this->db->set('cedula', $data['cedula']);
 $this->db->set('is_admin', $data['is_admin']);
 $this->db->set('email', $data['email']);
 $this->db->where('id', $data['id']);
 $this->db->update('student');
 }
}
?>