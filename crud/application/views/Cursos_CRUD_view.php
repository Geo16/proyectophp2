<!DOCTYPE html>
<html>
<head>
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>"rel="stylesheet" media="screen">
  <link rel="stylesheet" type="text/css" href="<?= base_url('Estilo.css') ?>">
	<meta charset="utf-8" />

	<?php 
	foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
	color: blue;
	text-decoration: none;
	font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body class="body">
  <br><br>
  <div class="container">
    <div style="border: 5px solid #C5DCB7;" class="navbar-inner">
     <div class="navbar">
      <ul class="nav">
        <li><a href="http://localhost/proyectophp2/crud/index.php/Inicio">Inicio</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Profesor_CRUD/profesores">Profesores</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Aulas_CRUD/aulas">Aulas</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Carreras_CRUD/carreras">Carreras</a></li>
        <li class="active"><a href="http://localhost/proyectophp2/crud/index.php/Cursos_CRUD/cursos">Cursos</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Estudiante_CRUD/estudiantes">Estudiantes</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Usuario_CRUD/usuarios">Usuarios</a></li>
                <li><a href="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD">Crear Grupos</a></li>
      </ul>
      <ul class="nav navbar-nav pull-right">
       <li ><a href="<?= base_url('index.php/login/logout_ci') ?>">Salir</a></li>
     </ul>
   </div>
 </div>
</div>
  <div class="container">
    <h2 style="color:white;">Cursos</h2>
    <?php echo $output; ?>
  </div>
</body>
</html>
