<html>
<head>
	<link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>"rel="stylesheet" media="screen">
	<link rel="stylesheet" type="text/css" href="<?= base_url('Estilo.css') ?>">
</head>
<body class="body" >

	<div class="container" style="margin-top:3%;">
		<div style="border: 5px solid #C5DCB7;" class="navbar-inner">
			<div class="navbar">
				<ul class="nav">
					<li><a href="http://localhost/proyectophp2/crud/index.php/Inicio">Inicio</a></li>
					<li ><a href="http://localhost/proyectophp2/crud/index.php/Profesor_CRUD/profesores">Profesores</a></li>
					<li><a href="http://localhost/proyectophp2/crud/index.php/Aulas_CRUD/aulas">Aulas</a></li>
					<li><a href="http://localhost/proyectophp2/crud/index.php/Carreras_CRUD/carreras">Carreras</a></li>
					<li><a href="http://localhost/proyectophp2/crud/index.php/Cursos_CRUD/cursos">Cursos</a></li>
					<li ><a href="http://localhost/proyectophp2/crud/index.php/Estudiante_CRUD/estudiantes">Estudiantes</a></li>
					<li ><a href="http://localhost/proyectophp2/crud/index.php/Usuario_CRUD/usuarios">Usuarios</a></li>
					<li class="active"><a href="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD">Crear Grupos</a></li>
				</ul>
				<ul class="nav navbar-nav pull-right">
					<li ><a href="<?= base_url('index.php/login/logout_ci') ?>">Salir</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container" style="margin-top:2%;">
		<table class="table table-hover"  >
			<thead>
				<tr>
					<th>Grupo</th>
					<th>Curso</th>
					<th>Codigo</th>
					<th>Profesor</th>
					<th>Apellido</th>
					<th>Acciones</th>		
				</tr>
			</thead>
			<tbody>		
				<tr >
					<?php foreach ($datos as $u):?>
					<td ><?=$u->grupo_numero?></td>
					<td><?=$u->cNombre?></td>
					<td><?=$u->codigo?></td>
					<td><?=$u->nombre?></td>
					<td><?=$u->apellido?></td>
					<td>
						<form name="tabla" action="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD/Accion" method="POST">
							<input type="hidden" name="editar" value="<?=$u->id_grupo?>"/>
							<button class="btn btn-success"type="submit" value="Editar">Editar</button>
						</form>
					</td>
					<td>
						<form name="tabla" action="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD/Borrar" method="POST">
							<button class="btn btn-danger" name="Borrar" value="<?=$u->id_grupo?>">Borrar</button>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	<form action="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD/buscar" method="POST">
		<input  type="text" list="profesores" name="buscar" placeholder="Buscar"/>
		<button  class="btn btn-info ">Buscar</button>
		<datalist id="profesores">
			<?php foreach ($datos as $u):?>
			<option name="opcion" value="<?=$u->nombre?>"></option>"
		<?php endforeach;?>
	</datalist>
	<form action="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD" method="POST">
		<button  class="btn btn-info ">Todos</button>
	</form>
</form>

</div>

<div class="container" style="margin-top:3%;">
	<form  name="insertar" action="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD/Insertar" method="POST">			
		<table class="table table-hover">
			<tr>
				<h2>Crear Grupo</h2>
			</tr>
			<tr>
				<td>Grupo: </td>
				<td>				
					<input  type="text" list="grupo" name="grupo_numero" placeholder="Numero de grupo" required/>
					<datalist id="grupo">
						<?php foreach ($datos as $u):?>
						<option name="opcion" value="<?=$u->grupo_numero?>"></option>
					<?php endforeach;?>
				</datalist>	
			</td>
		</tr>
		<tr>
			<td>Curso: </td>
			<td>				
				<input  type="text" list="curso" name="ID_curso" placeholder="ID Curso" required/>
				<datalist id="curso">
					<?php foreach ($curso as $u):?>
					<option id="<?=$u->id_curso?>" name="opcion" value="<?=$u->id_curso?>"><?=$u->cNombre?> </option>
				<?php endforeach;?>
			</datalist>	
		</td>
	</tr>
	<tr>
		<td>Profesor: </td>
		<td>		
			<input  type="text" list="nombresP" name="ID_profesor" placeholder="ID Profesor" required/>
			<datalist id="nombresP">
				<?php foreach ($profe as $p):?>
				<option name="opcion" value="<?=$p->id_profesor?>"><?=$p->nombre?></option>
			<?php endforeach;?>
		</datalist>		
	</td>
</tr>
</table>
<button class="btn btn-success"  name="insertar">Agregar Nuevo</button>
</form>
</div>
</body>
</html>