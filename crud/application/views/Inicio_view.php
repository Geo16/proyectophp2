<!DOCTYPE html>
<html >
<head>
  <link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>"rel="stylesheet" media="screen">
  <link rel="stylesheet" type="text/css" href="<?= base_url('Estilo.css') ?>">
</head>
<body class="body">
  <br><br>
  <div class="container">
    <div style="border: 5px solid #C5DCB7;" class="navbar-inner">
     <div class="navbar">
      <ul class="nav">
        <li class="active"><a href="http://localhost/proyectophp2/crud/index.php/Inicio">Inicio</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Profesor_CRUD/profesores">Profesores</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Aulas_CRUD/aulas">Aulas</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Carreras_CRUD/carreras">Carreras</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Cursos_CRUD/cursos">Cursos</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Estudiante_CRUD/estudiantes">Estudiantes</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Usuario_CRUD/usuarios">Usuarios</a></li>
        <li><a href="http://localhost/proyectophp2/crud/index.php/Grupos_CRUD">Crear Grupos</a></li>
      </ul>
      <ul class="nav navbar-nav pull-right">
       <li ><a href="<?= base_url('index.php/login/logout_ci') ?>">Salir</a></li>
     </ul>
   </div>
 </div>
</div>
  <div class="container" style="margin-top:2%;">
   <br>
   <div style="border: 2px solid #C5DCB7;" id="myCarousel" class="carousel slide">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <!-- Carousel items -->
    <div class="carousel-inner">
      <div class="active item"><img src="<?= base_url('imagenes/bienvenido.png') ?>" height="200" width="1000"></div>
      <div class="item"><img src="<?= base_url('imagenes/bienvenido2.jpg') ?>"  height="200" width="1000"></div>
      <div class="item"><img src="<?= base_url('imagenes/bienvenido3.jpg') ?>"  height="200" width="1000"></div>
    </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
  </div>

</body>
<footer>
 <script src="<?= base_url('bootstrap/js/jquery.js') ?>"></script>
  <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>
</footer>
</html>