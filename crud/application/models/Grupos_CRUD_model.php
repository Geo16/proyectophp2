
<?php
class Grupos_CRUD_model extends CI_Model {

	function Grupos_CRUD_model() {
 parent::__construct(); //llamada al constructor de Model.
}

function getData() { 
	$datos= $this->db->query("SELECT profesor.id_profesor,curso.id_curso,grupo.id_grupo,grupo.grupo_numero,
		curso.codigo,curso.cNombre,profesor.nombre,profesor.apellido
		FROM grupo,curso,profesor
		WHERE grupo.profesor_id = profesor.id_profesor and grupo.curso_id = curso.id_curso");
 return $datos->result(); //devolvemos el resultado de lanzar la query.
}
function getProf() { 
	$prof= $this->db->query("SELECT id_profesor,nombre FROM profesor");

 return $prof->result(); //devolvemos el resultado de lanzar la query.
}
function getCurs() { 
	$curs= $this->db->query("SELECT id_curso,cNombre FROM curso");

 return $curs->result(); //devolvemos el resultado de lanzar la query.
}
//Funcion para buscar un profesor con todos los grupos que tiene asociados
function busqueda($nombre) { 
	$datos= $this->db->query("SELECT grupo.id_grupo,grupo.grupo_numero,curso.codigo,curso.cNombre,profesor.nombre,profesor.apellido
		FROM grupo,curso,profesor
		WHERE grupo.profesor_id = profesor.id_profesor and grupo.curso_id = curso.id_curso and profesor.nombre='$nombre'");
 return $datos->result(); //devolvemos el resultado de lanzar la query.
}
//funcion para insertar datos
function insert($data) {

	$this->db->where('grupo_numero',$data['grupo_numero']);
	$this->db->where('profesor_id', $data['ID_profesor']);
	$this->db->where('curso_id', $data['ID_curso']);
	$query = $this->db->get('grupo');
	if($query->num_rows() == 1)//validacion para que no inserten un dato que ya existe
	{ 
		echo "<script languaje='javascript'>alert('Ese grupo con ese profesor y ese curso ya existen')</script>";
	}else{
		$this->db->set('grupo_numero', $data['grupo_numero']);
		$this->db->set('profesor_id', $data['ID_profesor']);
		$this->db->set('curso_id', $data['ID_curso']);
		$this->db->insert('grupo');
	}
}
//funcion para borrar datos
function borrar ($id) {
	$this->db->where('id_grupo', $id);
	$this->db->delete('grupo');
	redirect(base_url().'index.php/Grupos_CRUD');
}
//funcion para obtener datos del grupo
function obtenerContacto($id) {
	$this->db->select('id_grupo,grupo_numero,profesor_id,curso_id');
	$this->db->from('grupo');
	$this->db->where('id_grupo = ' . $id);
	$contacto = $this->db->get();
	return $contacto->result();
}
//funcion para editar datos
function update($data) {

		$this->db->set('grupo_numero', $data['grupo_numero']);
		$this->db->set('profesor_id', $data['profesor_id']);
		$this->db->set('curso_id', $data['curso_id']);
		$this->db->where('id_grupo', $data['id_grupo']);
		$this->db->update('grupo');
	}
}
?>