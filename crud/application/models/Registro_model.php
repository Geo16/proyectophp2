<?php
class Registro_model extends CI_Model
{
    public function construct()
    {
        parent::__construct();
    }
    
    //realizamos la inserción de los datos y devolvemos el 
    //resultado al controlador para envíar el correo si todo ha ido bien
    function new_user($nombre,$apellido,$email,$cedula,$password,$tipo,$activo)
    {
       $data = array(
            'nombre' => $nombre,
            'apellido' => $apellido,
            'email' => $email,
            'cedula' => $cedula,
            'password' => $password,
            'tipo' => $tipo,
            'activo' => $activo
        );
        return $this->db->insert('usuario', $data);    
    }
    public function verificar($cedula)
    {
      $r = $this->db->query("SELECT * FROM usuario WHERE cedula = '$cedula' ");
      if ($r->num_rows() == 0)
       return false;
   else
       return true;
}
}