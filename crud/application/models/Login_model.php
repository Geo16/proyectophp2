
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Login_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function login_user($cedula,$password)
    {
        $this->db->where('cedula', $cedula);
        $this->db->where('password', $password);
        $this->db->where('activo', "si");
        $query = $this->db->get('usuario');

        if($query->num_rows() == 1)//validamos que el usuario exista en la base de datos
        { 
            return $query->row();
        }else{
            $this->session->set_flashdata('usuario_incorrecto','Los datos introducidos son incorrectos');
            redirect(base_url().'index.php/Login','refresh');
        }
    }
}