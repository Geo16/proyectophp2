<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->helper(array('url','form'));
        $this->load->database('default');
    }
    
    public function index()
    {    
        switch ($this->session->userdata('tipo')) {
            case '':
            $data['token'] = $this->token();
            $data['titulo'] = 'Login con roles de usuario en codeigniter';
            $this->load->view('login_view',$data);
            break;
            case 'administrativo':
            redirect(base_url().'index.php/Inicio');
            break;
            case 'estudiante':
            redirect(base_url().'index.php/Inicios');
            break;   
            case 'profesor':
            redirect(base_url().'index.php/Inicios');
            break;
            default:        
            $data['titulo'] = 'Login con roles de usuario en codeigniter';
            $this->load->view('Login_view',$data);
            break;        
        }

    }
    
    public function new_user()
    {
        if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token'))
        {
            
            $this->form_validation->set_rules('cedula', 'Identification Number',  'required|trim|xss_clean');
            $this->form_validation->set_rules('password', 'Your Password', 'required|trim|xss_clean');
            
            //lanzamos mensajes de error si es que los hay
            
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }else{
                $cedula = $this->input->post('cedula');
                $password = $this->input->post('password');
                $check_user = $this->Login_model->login_user($cedula,$password);
                if($check_user == TRUE)
                {
                    
                    $data = array(
                        'is_logued_in'     =>         TRUE,
                        'id'     =>         $check_user->id,
                        'nombre'        =>        $check_user->nombre,
                        'apellido'        =>        $check_user->apellido,
                        'email'        =>        $check_user->email,
                        'tipo'        =>        $check_user->tipo,
                        'activo'        =>        $check_user->activo,
                        'cedula'         =>         $check_user->cedula);        
                    $this->session->set_userdata($data);
                    $this->index();
                    
                }
            }
        }else{
            redirect(base_url().'index.php/login');
        }
    }
    
    public function token()
    {
        $token = md5(uniqid(rand(),true));
        $this->session->set_userdata('token',$token);
        return $token;
    }
    
    public function logout_ci()
    {
        $this->session->sess_destroy();
        $this->index();
    }
}

