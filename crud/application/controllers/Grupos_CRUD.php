<?php 

class Grupos_CRUD extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	function index() {
 $this->load->model('Grupos_CRUD_model'); //cargamos el modelo
 
 $data['page_title'] = " ";
 //Obtener datos de la tabla 'grupo'
 $datos = $this->Grupos_CRUD_model->getData(); //llamamos a la función getData() del modelo creado anteriormente.
 $prof= $this->Grupos_CRUD_model->getProf(); //llamamos a la función getProf() del modelo creado anteriormente.
 $curs= $this->Grupos_CRUD_model->getCurs(); //llamamos a la función getCurs() del modelo creado anteriormente.
 $data['datos'] = $datos;
 $data['profe'] = $prof;
 $data['curso'] = $curs;

 //load de vistas
 $this->load->view('Grupos_CRUD_view', $data); //llamada a la vista, que crearemos posteriormente
}
function buscar() {
 //recogemos los datos obtenidos por POST
	
	if ($buscar= $_POST['buscar']== "") {
		redirect(base_url().'index.php/Grupos_CRUD');
	}else{
		$buscar= $_POST['buscar'];
		$this->load->model('Grupos_CRUD_model');
		$datos= $this->Grupos_CRUD_model->busqueda($buscar);
 //volvemos a visualizar la tabla
		$data['datos']=$datos;
		$this->load->view('Grupos_CRUD_view', $data);
	}
}
function Insertar() {
 //recogemos los datos obtenidos por POST
	$data['grupo_numero'] = $_POST['grupo_numero'];
	$data['ID_curso'] = $_POST['ID_curso'];
	$data['ID_profesor'] = $_POST['ID_profesor'];
 //llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
	$this->load->model('Grupos_CRUD_model');
	$this->Grupos_CRUD_model->insert($data);
 //volvemos a visualizar la tabla
	$this->index();

}

function Borrar() {
 //obtenemos el nombre
 //cargamos el modelo y llamamos a la función borrar(), pasándole el id del registro que queremos borrar.
	$id=$_POST['Borrar'];
	$this->load->model('Grupos_CRUD_model');
	$this->Grupos_CRUD_model->borrar($id);
 //mostramos la vista de nuevo.
	$this->index();
}
function accion() {
 //cargamos el modelo y obtenemos la información del contacto seleccionado.
	$this->load->model('Grupos_CRUD_model');
	$data['datos'] = $this->Grupos_CRUD_model->obtenerContacto($_POST['editar']);
 //cargamos la vista para editar la información, pasandole dicha información.
	$this->load->view('Grupos_CRUD_view2', $data);
	
}

function editar() {
 //recogemos los datos por POST
	$data['id_grupo'] = $_POST['id_grupo'];
	$data['grupo_numero'] = $_POST['grupo_numero'];
	$data['profesor_id'] = $_POST['ID_profesor'];
	$data['curso_id'] = $_POST['ID_curso'];
 //cargamos el modelo y llamamos a la función update()
	$this->load->model('Grupos_CRUD_model');
	$this->Grupos_CRUD_model->update($data);
 //volvemos a cargar la primera vista
	$this->index();
}

}
