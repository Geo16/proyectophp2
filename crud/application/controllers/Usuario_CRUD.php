<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase CI_Controller */
class Usuario_CRUD extends CI_Controller {

	function __construct() 
	{
		
		parent::__construct();
	}

	function index() 
	{
		/*
		 * Mandamos todo lo que llegue a la funcion
		 * administracion().
		 **/
		redirect('Usuario_CRUD_view');
	}

	/*
	 * 
 	 **/
	function Usuarios()
	{
		try{

			/* Creamos el objeto */
			$crud = new grocery_CRUD();

			/* Seleccionamos el tema */
			$crud->set_theme('flexigrid');

			/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
			$crud->set_table('usuario');

			/* Le asignamos un nombre */
			$crud->set_subject('Usuarios');

			/* Asignamos el idioma español */
			$crud->set_language('spanish');

			/* Aqui le decimos a grocery que estos campos son obligatorios */
			$crud->required_fields(
				'id',
				'nombre',
				'apellido',
				'password',
				'cedula',
				'tipo',
				'email',
				'activo'
				);

			/* Aqui le indicamos que campos deseamos mostrar */
			$crud->columns(
				'id',
				'nombre',
				'apellido',
				'password',
				'cedula',
				'tipo',
				'email',
				'activo'
				);
			
			/* Generamos la tabla */
			$output = $crud->render();
			
			/* La cargamos en la vista */
			$this->load->view('Usuario_CRUD_view', $output);
			
		}catch(Exception $e){
			/* Si algo sale mal cachamos el error y lo mostramos */
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}