<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * 
 */
class Inicios extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url','form'));

    }
    
    public function index()
    {
        if($this->session->userdata('tipo') == FALSE || $this->session->userdata('tipo') == 'Estudiante')
        {
            redirect(base_url().'index.php/Login');
        }
        if($this->session->userdata('tipo') == FALSE || $this->session->userdata('tipo') == 'Profesor')
        {
            redirect(base_url().'index.php/Login');
        }
        $data['titulo'] = 'Bienvenido de nuevo ' .$this->session->userdata('tipo');
        $this->load->view('Inicios_view',$data);
    }
}