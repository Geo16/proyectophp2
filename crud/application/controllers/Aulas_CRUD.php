<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase CI_Controller */
class Aulas_CRUD extends CI_Controller {

	function __construct() 
	{
		
		parent::__construct();
	}

	function index() 
	{
		/*
		 * Mandamos todo lo que llegue a la funcion
		 * Aulas().
		 **/
		redirect('Aulas_CRUD_view');
	}

	/*
	 * 
 	 **/
	function Aulas()
	{
		try{

			/* Creamos el objeto */
			$crud = new grocery_CRUD();

			/* Seleccionamos el tema */
			$crud->set_theme('flexigrid');

			/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
			$crud->set_table('aulas');

			/* Le asignamos un nombre */
			$crud->set_subject('Aulas');

			/* Asignamos el idioma español */
			$crud->set_language('spanish');

			/* Aqui le decimos a grocery que estos campos son obligatorios */
			$crud->required_fields(
				'id_aulas',
				'codigo',
				'nombre',
				'ubicacion'
				);

			/* Aqui le indicamos que campos deseamos mostrar */
			$crud->columns(
				'id_aulas',
				'codigo',
				'nombre',
				'ubicacion'
				);
			
			/* Generamos la tabla */
			$output = $crud->render();
			
			/* La cargamos en la vista */
			$this->load->view('Aulas_CRUD_view', $output);
			
		}catch(Exception $e){
			/* Si algo sale mal cachamos el error y lo mostramos */
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}