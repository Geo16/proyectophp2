/*
Para el uso con RAILS colocar los nombres de las tablas y llaves foráneas en plural
De acuerdo a la base de datos utilizada la sintaxis del AUTO_INCREMENT puede variar
http://www.w3schools.com/sql/sql_AUTO_INCREMENT.asp
Los tipos de dato también pueden varias entre bases de datos, se recomienda utilizar
campos ENUM o BOOLEAN para los campos bandera cuando la base de datos lo permita:
professor.is_admin, student_id_admin, group.enabled => boolean
test.status => ENUM('inactive','active','finished');
*/
CREATE TABLE usuario (
	id_usuario INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre varchar(255) NOT NULL,
	apellido varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	cedula varchar(255) NOT NULL,
	tipo varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	activo varchar(255) NOT NULL);

CREATE TABLE profesor (
	id_profesor INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre varchar(255) NOT NULL,
	apellido varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	cedula varchar(255) NOT NULL,
	email varchar(255) NOT NULL);

CREATE TABLE aulas(
	id_aulas INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	codigo varchar(255) NOT NULL,
	nombre varchar(255) NOT NULL,
	ubicacion varchar(255) NOT NULL);

CREATE TABLE carrera(
	id_carrera INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	codigo varchar(255) NOT NULL,
	nombre varchar(255) NOT NULL,
	descripcion varchar(255) NOT NULL);

CREATE TABLE estudiante (
	id_estudiante INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre varchar(255) NOT NULL,
	apellido varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	cedula varchar(255) NOT NULL,
	email varchar(255) NOT NULL);

CREATE TABLE curso (
	id_curso INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	codigo varchar(255) NOT NULL,
	cNombre varchar(255) NOT NULL,
	descripcion varchar(255),
	carrera_id integer NOT NULL REFERENCES carrera(id) ON DELETE RESTRICT);

CREATE TABLE grupo (
	id_grupo INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	grupo_numero integer DEFAULT 0,
	profesor_id integer NOT NULL REFERENCES professor(id) ON DELETE RESTRICT,
	curso_id integer NOT NULL REFERENCES course(id) ON DELETE RESTRICT);

CREATE TABLE registration (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	group_id INTEGER NOT NULL REFERENCES `group`(id) ON DELETE CASCADE,
	student_id INTEGER NOT NULL REFERENCES student(id) ON DELETE CASCADE,
	CONSTRAINT pk_registration UNIQUE (group_id, student_id)
	);

CREATE TABLE test (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	group_id INTEGER NOT NULL REFERENCES `group`(id) ON DELETE CASCADE,
	description varchar(255),
	application_date datetime,
	status integer DEFAULT 0,
	term_in_minutes integer DEFAULT 0,
	percent integer DEFAULT 0,
	comments text,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE notification_sent (
  id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  student_id INTEGER NOT NULL REFERENCES student(id) ON DELETE RESTRICT,
  test_id INTEGER NOT NULL REFERENCES test(id) ON DELETE RESTRICT);






