<?php
class Registro_model extends CI_Model
{
    public function construct()
    {
        parent::__construct();
    }
    
    //realizamos la inserción de los datos y devolvemos el 
    //resultado al controlador para envíar el correo si todo ha ido bien
    function new_user($nombre,$apellido,$email,$cedula,$password)
    {
       $data = array(
            'nombre' => $nombre,
            'apellido' => $apellido,
            'email' => $email,
            'cedula' => $cedula,
            'password' => $password
        );
        return $this->db->insert('usuario', $data);    
    }
}