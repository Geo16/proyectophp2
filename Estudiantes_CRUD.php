<?php 
 
class Estudiantes_CRUD extends CI_Controller {
 
 public function __construct() {
 parent::__construct();
 }
 
 function index() {
 $this->load->model('Estudiantes_CRUD_model'); //cargamos el modelo
 
 $data['page_title'] = " ";
 
 //Obtener datos de la tabla 'Estudiantes'
 $estudiantes = $this->Estudiantes_CRUD_model->getData(); //llamamos a la función getData() del modelo creado anteriormente.
 
 $data['estudiantes'] = $estudiantes;
 
 //load de vistas
 $this->load->view('Estudiantes_CRUD_view', $data); //llamada a la vista, que crearemos posteriormente
 }
function Insertar() {
 //recogemos los datos obtenidos por POST
 $data['nombre'] = $_POST['txtNombre'];
 $data['apellido'] = $_POST['txtApellido'];
 $data['password'] = $_POST['txtPassword'];
 $data['cedula'] = $_POST['txtCedula'];
 $data['is_admin'] = $_POST['txtIsadmin'];
 $data['email'] = $_POST['txtEmail'];
 //llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
 $this->load->model('Estudiantes_CRUD_model');
 $this->Estudiantes_CRUD_model->insert($data);
 //volvemos a visualizar la tabla
 $this->index();
 }

 function Borrar() {
 //obtenemos el nombre
 //cargamos el modelo y llamamos a la función baja(), pasándole el nombre del registro que queremos borrar.
 $id=$_POST['Borrar'];
 $this->load->model('Estudiantes_CRUD_model');
 $this->Estudiantes_CRUD_model->borrar($id);
 //mostramos la vista de nuevo.
 $this->index();
 }
 function accion() {
 //cargamos el modelo y obtenemos la información del contacto seleccionado.
 $this->load->model('Estudiantes_CRUD_model');
 $data['estudiantes'] = $this->Estudiantes_CRUD_model->obtenerContacto($_POST['editar']);
 //cargamos la vista para editar la información, pasandole dicha información.
 $this->load->view('Estudiantes_CRUD_view2', $data);
 }
 
 function editar() {
 //recogemos los datos por POST
 $data['id'] = $_POST['id'];
 $data['nombre'] = $_POST['txtNombre'];
 $data['apellido'] = $_POST['txtApellido'];
 $data['password'] = $_POST['txtPassword'];
 $data['cedula'] = $_POST['txtCedula'];
 $data['is_admin'] = $_POST['txtIsadmin'];
 $data['email'] = $_POST['txtEmail'];
 //cargamos el modelo y llamamos a la función update()
 $this->load->model('Estudiantes_CRUD_model');
 $this->Estudiantes_CRUD_model->update($data);
 //volvemos a cargar la primera vista
 $this->index();
 }
 
}
 